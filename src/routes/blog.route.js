const express = require('express');
const {
    createBlog, comment
} = require('../controllers/blog');

const router = express.Router();

router.post('/create', createBlog);
router.post('/comment', comment)

module.exports = router;
