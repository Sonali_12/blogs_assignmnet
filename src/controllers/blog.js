const { User, Blog, Comment } = require('../models');

/**
 * @desc   Create blog
 * @route  /api/blog/create
 * @access private
 */
exports.createBlog = async (req, res) => {
    try {
        Blog.create(req.body).then(data => {
            return res.status(200).json({
                success: true,
                message: 'Blog created suessfully',
                data: data
            })
        }).catch(error => {
            return res.status(500).json({
                success: false,
                error: `Something went wrong ${error}`
            })
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: `Something went wrong ${error}`
        })
    }
}


/**
 * @desc   Comment on blog
 * @route  /api/blog/comment
 * @access Public
 */
exports.comment = async (req, res) => {
    try {
        Comment.create(req.body).then(data => {
            return res.status(200).json({
                success: true,
                message: 'Comment created suessfully',
                data: data
            })
        }).catch(error => {
            return res.status(500).json({
                success: false,
                error: `Something went wrong ${error}`
            })
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: `Something went wrong ${error}`
        })
    }
}
