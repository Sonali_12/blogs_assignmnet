const { User, Blog, Comment } = require('../models');

/**
 * @desc   User registerations
 * @route  /api/user/register
 * @access Public
 */
exports.register = async (req, res) => {
    try {
        User.create(req.body).then(data => {
            return res.status(200).json({
                success: true,
                message: 'User registered suessfully',
                data: data
            })
        }).catch(error => {
            return res.status(500).json({
                success: false,
                error: `Something went wrong ${error}`
            })
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: `Something went wrong ${error}`
        })
    }
}


/**
 * @desc   User login
 * @route  /api/user/login
 * @access Public
 */
exports.login = async (req, res) => {
    try {
        const { email, password } = req.body
        if (!email || !password) {
            return res.status(500).json({
                success: false,
                error: 'Enter email and password'
            })
        }

        User.findOne({ email }).select('+password')
            .then(async (user) => {
                if (!user) {
                    return res.status(401).json({
                        success: false,
                        error: 'User does not exist with this email id'
                    })
                }

                if (user.password === password) {
                    return res.status(200).json({
                        success: true,
                        message: 'User login suessfully',
                        data: user
                    })

                }
                return res.status(401).json({
                    success: false,
                    error: 'Invalid credentials'
                })
            }).catch(error => {
                return res.status(500).json({
                    success: false,
                    error: `Something went wrong ${error}`
                })
            })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: `Something went wrong ${error}`
        })
    }
}

