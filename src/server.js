const express = require('express');
const path = require('path');
const cors = require('cors');
const dotenv = require('dotenv');

/** Load environment variables */
dotenv.config({ path: `config.env` });

const connectDB = require('./config/db');

/** Require routes files */
const user = require('./routes/user.route');
const blog = require('./routes/blog.route');

const app = express();

/** Middleware */
app.use(cors());
app.use(express.json({ limit: '5mb' }));

/** Mount App Routes */
app.use('/api/user', user);
app.use('/api/blog', blog);

/** Set up mongoose connection */
connectDB();

const PORT = process.env.PORT;

const server = app.listen(PORT, () =>
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);

/** Handle unhandled promise rejections */
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error : ${err.message}`);

  /** Close server */
  server.close(() => process.exit(1));
});

/** Test route */
app.get('/', async (req, res) => {
  res.send(`blogs is running on ${PORT}`);
});

action()
async function action() {

  const { User, Blog, Comment } = require('./models');

  const blogs = await Blog.distinct('_id', { user: '60a64d3adcca6a42efb24830' })

  let blogsWithUsers = []
  for (i = 0; i < blogs.length; i++) {

    const countofcoments = await Blog.aggregate([
      {
        $group: { _id: { user: '$user' }, count: { $sum: 1 } }
      },
      {
        $project: { _id: 0, user: '$_id.user', count: '$count' }
      },
      // { $sort: { name: 1 } }
    ]);

    console.log(countofcoments)
    // let json = {}
    // const commentedUsers = await Comment.distinct('userid', { blogid: blogs[i]._id })
    // json['blog'] = blogs[i]._id
    // json['comments'] = commentedUsers
    // blogsWithUsers.push(json)
  }
}