const mongoose = require('mongoose');

const BlogUserSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: ['enter username', true]
        },
        email: {
            type: String,
            unique: true,
            match: [
                /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                'Please add a valid email',
            ],
            required: ['enter email', true]
        },
        password: {
            type: String,
            required: ['enter password', true]
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true },
        timestamps: true
    }
);

/** Reverse populate with virtuals */
BlogUserSchema.virtual('blogs', {
    ref: 'Blog',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

BlogUserSchema.virtual('mycomments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'userid',
    justOne: false,
});

const User = mongoose.model('User', BlogUserSchema);
module.exports = User;
