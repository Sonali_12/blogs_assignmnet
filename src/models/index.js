const
    User = require('./users'),
    Blog = require('./blogs'),
    Comment = require('./comments');

module.exports = {
    User,
    Blog,
    Comment
}