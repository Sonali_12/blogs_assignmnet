const mongoose = require('mongoose');

const CommentShema = new mongoose.Schema(
    {
        message: {
            type: String,
            required: ['enter message', true]
        },
        blogid: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Blog',
            required: ['enter blogid', true]
        },
        userid: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: ['enter userid', true]
        }
    },
    {
        timestamps: true
    }
);
const Comment = mongoose.model('Comment', CommentShema);
module.exports = Comment;
