const mongoose = require('mongoose');

const BlogSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: ['enter Title', true]
        },
        content: {
            type: String,
            required: ['enter Content', true]
        },
        publishDate: {
            type: Date
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: ['enter userid', true]
        },


        // comments: [{
        //     messgae: {
        //         type: String
        //     },
        //     user: {
        //         type: mongoose.Schema.Types.ObjectId,
        //         ref: 'User'
        //     }
        // }]
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true },
        timestamps: true
    }
);

/** Reverse populate with virtuals */
BlogSchema.virtual('comments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'blogid',
    justOne: false,
});

const Blog = mongoose.model('Blog', BlogSchema);
module.exports = Blog;
